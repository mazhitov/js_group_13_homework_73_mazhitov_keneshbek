const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('hello');
});

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.listen(port, () => {
    console.log('we are live on ' + port);
});
