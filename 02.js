const expressVigenere = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const appVigenere = expressVigenere();
const portVigenere = 8000;
const key = 'password';

appVigenere.get('/', (req, res) => {
    res.send('hellobro');
});

appVigenere.get('/encode/:name', (req, res) => {
    res.send(Vigenere.Cipher(key).crypt(req.params.name));
});
appVigenere.get('/decode/:name', (req, res) => {
    res.send(Vigenere.Decipher(key).crypt(req.params.name));
});
appVigenere.listen(portVigenere, () => {
    console.log('we are live  ' + portVigenere);
});





// Vigenere.Decipher(key).crypt('pbu-0123456789@wtodsae.ugi');
